#include <iostream>
#include <cstdlib>
#include <cerrno>
#include <iomanip>
#include <utility>
#include <vector>
#include <string>

#include "RODBusy/RODBusy.h"

#include "rcc_time_stamp/tstamp.h"
#include "rcc_error/rcc_error.h"

using namespace RCD;

bool RODBusy::CheckCERNID() {
  u_int man, rev, id;
  this->ReadConfigurationEEPROM(man, rev, id);
  return ( 0x00080030 == man)? true : false;
}

// performs basic test to increase the likeliness that the vme
// module is actually an RODBusy module
bool RODBusy::CheckRODBusy() {
  bool ret = true;

  // first check CERN ID
  if (!(this->CheckCERNID())) {
    return false;
  }

  // add additional checks here

  return ret;
}


int RODBusy::ReadConfigurationEEPROM(u_int& board_man, u_int& board_revision, u_int& board_id) {

  int status = 0;
  u_short rev1, rev2, rev3, rev4;
  u_short id1, id2, id3, id4;
  u_short man1, man2, man3;

  status |= m_vmm->ReadSafe(0x4e,&rev1);
  status |= m_vmm->ReadSafe(0x4a,&rev2);
  status |= m_vmm->ReadSafe(0x46,&rev3);
  status |= m_vmm->ReadSafe(0x42,&rev4);

  status |= m_vmm->ReadSafe(0x3e,&id1);
  status |= m_vmm->ReadSafe(0x3a,&id2);
  status |= m_vmm->ReadSafe(0x36,&id3);
  status |= m_vmm->ReadSafe(0x32,&id4);

  status |= m_vmm->ReadSafe(0x2e,&man1);
  status |= m_vmm->ReadSafe(0x2a,&man2);
  status |= m_vmm->ReadSafe(0x26,&man3);

  u_int urev1 = static_cast<u_int>(rev1 & 0xff);
  u_int urev2 = (static_cast<u_int>(rev2 & 0xff) << 8) & 0xff00;
  u_int urev3 = (static_cast<u_int>(rev3 & 0xff) << 16) & 0xff0000;
  u_int urev4 = (static_cast<u_int>(rev4 & 0xff) << 24) & 0xff000000;
  board_revision = urev4 | urev3 | urev2 | urev1;

  u_int uid1 = static_cast<u_int>(id1 & 0xff);
  u_int uid2 = (static_cast<u_int>(id2 & 0xff) << 8) & 0xff00;
  u_int uid3 = (static_cast<u_int>(id3 & 0xff) << 16) & 0xff0000;
  u_int uid4 = (static_cast<u_int>(id4 & 0xff) << 24) & 0xff000000;
  board_id = uid4 | uid3 | uid2 | uid1;

  
  u_int uman1 = static_cast<u_int>(man1 & 0xff);
  u_int uman2 = (static_cast<u_int>(man2 & 0xff) << 8) & 0xff00;
  u_int uman3 = (static_cast<u_int>(man3 & 0xff) << 16) & 0xff0000;
  board_man = uman3 | uman2 | uman1;

//   printf(" Manufacturer: 0x%08x \n", board_man);
//   printf(" Revision    : 0x%08x \n", board_revision);
//   printf(" Id          : 0x%08x \n", board_id);

  return status;
}
 

//------------------------------------------------------------------------------
std::string RODBusy::GetError(int errcode) {

  char message1[100];
  sprintf(message1,"Error %d in package RODBusy => ",errcode);
  std::string errstring = message1;
  if (errcode == ERROR_IRQ_LEVEL_OUTOFRANGE) {
    errstring += "IRQ level out of range";
  }
  else if (errcode ==  ERROR_FIFOS_EMPTY) {
    errstring += "FIFOs empty";
  }
  else if (errcode ==  ERROR_CHANNEL_OUTOFRANGE) {
    errstring += "Channel out of range";
  }
  else {
    m_vme->ErrorString(errcode, &errstring);
  }

  return errstring;
}

//------------------------------------------------------------------------------
int RODBusy::Reset() {
  u_short data = 1;
  u_short reg = REG_SWRST;

  // write register
  return m_vmm->WriteSafe(reg,data);
}


//------------------------------------------------------------------------------
int RODBusy::Open(u_int base) {

  // open VME library
  m_vme = VME::Open();

  // create VME master mapping
  m_vmm = m_vme->MasterMap(base,VME_SIZE,VME_A24,0);
  return (*m_vmm)();
}

//------------------------------------------------------------------------------
int RODBusy::Close(void) {

  // unmap VME master mapping
  int status = m_vme->MasterUnmap(m_vmm);

  // close VME library
  VME::Close();

  return status;
}

//------------------------------------------------------------------------------
int RODBusy::setBits(u_int reg, u_short mask, u_short bits) {
  u_short data = 0;
  int status = 0;
  // read register
  if ( (status = m_vmm->ReadSafe(reg,&data)) != VME_SUCCESS) {
    return status;
  }
     
  // set bits
  data = (data & ~mask) | ( bits & mask);

  // write register
  return m_vmm->WriteSafe(reg,data);
}

//------------------------------------------------------------------------------
int RODBusy::testBits(u_int reg, u_short mask, bool& result) {
  u_short data;
  int status = 0;
  // read register
  if ( (status = m_vmm->ReadSafe(reg,&data)) != VME_SUCCESS) {
    return status;
  }

  // apply mask
  data = (data & mask);
  if (data>0) result=true;
  else result= false;

  return(status);
}

//------------------------------------------------------------------------------
int RODBusy::getBits(u_int reg, u_short mask, u_short& data) {

  u_short d;
  int status = 0;
  // read register
  status = m_vmm->ReadSafe(reg,&d);
     
  //  apply read mask
  data = (d & mask);
  return status;
}


//------------------------------------------------------------------------------
int RODBusy::provokeInterrupt() {
  u_short data = 1;
  u_short reg = REG_SWIRQ;

  // write register
  return m_vmm->WriteSafe(reg,data);
}

//------------------------------------------------------------------------------
int RODBusy::set_statusID(u_short id) {
  return setBits(REG_INTID, MSK_INTID_VECTOR, id);
}

//------------------------------------------------------------------------------
int RODBusy::get_statusID(u_short& id) {
  return getBits(REG_INTID, MSK_INTID_VECTOR, id);
}

    
//------------------------------------------------------------------------------
int RODBusy::set_IRQ_level(u_short l) {
  int ret = 0;
  if ( (l>7) || (l<1) ) {
    ret = ERROR_IRQ_LEVEL_OUTOFRANGE;
    ret |= setBits(REG_INTCSR, MSK_INTCSR_IRQ_W, l);
  }
  else {
    ret = setBits(REG_INTCSR, MSK_INTCSR_IRQ_W, l);
  }
  return(ret);
}

//------------------------------------------------------------------------------
int RODBusy::get_IRQ_level(u_short& l) {
  return getBits(REG_INTCSR, MSK_INTCSR_IRQ_R, l);
}

    
//------------------------------------------------------------------------------
int RODBusy::enable_IRQ() {
  return setBits(REG_INTCSR, MSK_INTCSR_IRQ_EN, INTCSR_IRQ_EN);
}

//------------------------------------------------------------------------------
int RODBusy::disable_IRQ() {
  return setBits(REG_INTCSR, MSK_INTCSR_IRQ_EN, INTCSR_IRQ_DIS);
}


//------------------------------------------------------------------------------
bool RODBusy::IRQ_is_enabled() {
  bool ret(false);
  testBits(REG_INTCSR, MSK_INTCSR_IRQ_EN, ret);
  return ret;
}

//------------------------------------------------------------------------------
bool RODBusy::IRQ_is_disabled() {
  return !(this->IRQ_is_enabled());
}

//------------------------------------------------------------------------------
bool RODBusy::IRQ_is_pending() {
  bool ret(false);
  testBits(REG_INTCSR, MSK_INTCSR_IRQ_PEND, ret);
  return ret;
}

//------------------------------------------------------------------------------
bool RODBusy::SREQ_is_pending() {
  bool ret(false);
  testBits(REG_INTCSR, MSK_INTCSR_SREQ_PEND, ret);
  return ret;
}

//------------------------------------------------------------------------------
bool RODBusy::is_busy() {
  bool ret(false);
  testBits(REG_SREQCSR, MSK_SREQCSR_BSY_STATUS, ret);
  return ret;
}

//------------------------------------------------------------------------------
bool RODBusy::is_not_busy() {
  return !(this->is_busy());
}

//------------------------------------------------------------------------------
int RODBusy::assert_sw_busy() {
  return setBits(REG_SREQCSR, MSK_SREQCSR_SWBUSY, SREQCSR_SWBUSY_EN);
}

//------------------------------------------------------------------------------
int RODBusy::release_sw_busy() {
  return setBits(REG_SREQCSR, MSK_SREQCSR_SWBUSY, SREQCSR_SWBUSY_DIS);
}

//------------------------------------------------------------------------------
bool RODBusy::sw_busy_is_asserted() {
  bool ret(false);
  testBits(REG_SREQCSR, MSK_SREQCSR_SWBUSY, ret);
  return ret;
}

//------------------------------------------------------------------------------
int RODBusy::enable_SREQ() {
  return setBits(REG_SREQCSR, MSK_SREQCSR_SREQ_EN, SREQCSR_SREQ_EN);
}

//------------------------------------------------------------------------------
int RODBusy::disable_SREQ() {
  return setBits(REG_SREQCSR, MSK_SREQCSR_SREQ_EN, SREQCSR_SREQ_DIS);
}

//------------------------------------------------------------------------------
bool RODBusy::SREQ_is_enabled() {
  bool ret(false);
  testBits(REG_SREQCSR, MSK_SREQCSR_SREQ_EN, ret);
  return ret;
}

//------------------------------------------------------------------------------
bool RODBusy::SREQ_is_active() {
  bool ret(false);
  testBits(REG_SREQCSR, MSK_SREQCSR_SREQ_ACT, ret);
  return ret;
}

//------------------------------------------------------------------------------
int RODBusy::assert_SREQ() {
  return setBits(REG_SREQCSR, MSK_SREQSETCLR, SREQSETCLR_SET);
}

//------------------------------------------------------------------------------
int RODBusy::clear_SREQ() {
  return setBits(REG_SREQCSR, MSK_SREQSETCLR, SREQSETCLR_CLR);
}

   
//------------------------------------------------------------------------------
int RODBusy::setLimit(u_short l) {
  u_short reg = REG_LIMREG;
  return m_vmm->WriteSafe(reg,l);
}

//------------------------------------------------------------------------------
int RODBusy::getLimit(u_short& l) {
  u_short reg = REG_LIMREG;
  return m_vmm->ReadSafe(reg,&l);
}


//------------------------------------------------------------------------------
int RODBusy::setResetInterval(u_short l) {
  u_short reg = REG_IVALREG;
  return m_vmm->WriteSafe(reg,l);
}

//------------------------------------------------------------------------------
int RODBusy::getResetInterval(u_short& l) {
  u_short reg = REG_IVALREG;
  return m_vmm->ReadSafe(reg,&l);
}


//------------------------------------------------------------------------------
int RODBusy::getBusyState(u_short& l) {
  u_short reg = REG_BUSYSTATE;
  return m_vmm->ReadSafe(reg,&l);
}

//------------------------------------------------------------------------------
int RODBusy::setBusyState(u_short l) {
  u_short reg = REG_BUSYSTATE;
  return m_vmm->WriteSafe(reg,l);
}

//------------------------------------------------------------------------------
int RODBusy::getBusyState(bool& b, u_short channel) {
  u_short reg = REG_BUSYSTATE;
  u_short mask = 0x1<<channel;
  return this->testBits(reg, mask, b);
}

//------------------------------------------------------------------------------
int RODBusy::setBusyState(bool b, u_short channel) {
  u_short reg = REG_BUSYSTATE;
  u_short mask = 0x1<<channel;
  u_short bits = b ? mask : 0;
  return this->setBits(reg, mask, bits);
}

//------------------------------------------------------------------------------
int RODBusy::setBusyMask(u_short l) {
  u_short reg = REG_BUSYMASK;
  return m_vmm->WriteSafe(reg,l);
}

//------------------------------------------------------------------------------
int RODBusy::getBusyMask(u_short& l) {
  u_short reg = REG_BUSYMASK;
  return m_vmm->ReadSafe(reg,&l);
}

//------------------------------------------------------------------------------
int RODBusy::enableChannel(u_short channel) {
  u_short reg = REG_BUSYMASK;
  u_short mask = 0x1<<channel;
  return this->setBits(reg, mask, mask);
}

//------------------------------------------------------------------------------
int RODBusy::disableChannel(u_short channel) {
  u_short reg = REG_BUSYMASK;
  u_short mask = 0x1<<channel;
  u_short data = 0x0;
  return this->setBits(reg, mask, data);
}

//------------------------------------------------------------------------------
bool RODBusy::channelIsEnabled(u_short channel) {
  u_short reg = REG_BUSYMASK;
  u_short mask = 0x1<<channel;
  bool b(false);
  this->testBits(reg, mask, b);
  return b;
}


//------------------------------------------------------------------------------
int RODBusy::resetCounter() {
  u_short reg = REG_CNTRST;
  u_short data = 0x1;
  return m_vmm->WriteSafe(reg,data);
}

//------------------------------------------------------------------------------
int RODBusy::resetFifos() {
  u_short reg = REG_FIFRST;
  u_short data = 0x1;
  return m_vmm->WriteSafe(reg,data);
}

//------------------------------------------------------------------------------
int RODBusy::transferCountersToFifos() {
  u_short reg = REG_FIFWEN;
  u_short data = 0x1;
  return m_vmm->WriteSafe(reg,data);
}

//------------------------------------------------------------------------------
int RODBusy::setMode(enum fifomode m) {
  u_short reg = REG_FIFWCR;
  u_short data(0);
  switch (m) {
  case SEQ_COUNT_DISABLED:
    data = 0;
    break;
  case SEQ_DISABLED_COUNT_ENABLED:
    data = 1;
    break;
  case SEQ_IDLE_COUNT_DISABLED:
    data = 2;
    break;
  case SEQ_COUNT_ENABLED: 
    data = 3;
    break;
  }
  return m_vmm->WriteSafe(reg,data);
}

//------------------------------------------------------------------------------
int RODBusy::getMode(enum fifomode& m) {
  u_short reg = REG_FIFWCR;
  u_short data(0);
  int ret = m_vmm->ReadSafe(reg,&data);
  data = data&0x3;
  switch (data) {
  case 0:
    m = SEQ_COUNT_DISABLED;
    break;
  case 1:
    m = SEQ_DISABLED_COUNT_ENABLED;
    break;
  case 2:
    m = SEQ_IDLE_COUNT_DISABLED;
    break;
  case 3: 
    m = SEQ_COUNT_ENABLED;
    break;
  }
  return ret;
}


//------------------------------------------------------------------------------
int RODBusy::setFifoReadingByVME(u_short hex) {
  u_short reg = REG_FIFRCR;
  return m_vmm->WriteSafe(reg,hex);
}

//------------------------------------------------------------------------------
int RODBusy::getFifoReadingByVME(u_short& hex) {
  u_short reg = REG_FIFRCR;
  return m_vmm->ReadSafe(reg,&hex);
}

//------------------------------------------------------------------------------
int RODBusy::getSequencerTransferInterval(u_short& interval) {
  u_short reg = REG_SEQREG;
  return m_vmm->ReadSafe(reg,&interval);
}

//------------------------------------------------------------------------------
int RODBusy::setSequencerTransferInterval(u_short interval) {
  u_short reg = REG_SEQREG;
  return m_vmm->WriteSafe(reg,interval);
}

//------------------------------------------------------------------------------
int RODBusy::getFifoFullFlags(u_short& flags) {
  u_short reg = REG_FIFFF;
  return m_vmm->ReadSafe(reg,&flags);
}

//------------------------------------------------------------------------------
int RODBusy::getFifoEmptyFlags(u_short& flags) {
  u_short reg = REG_FIFEF;
  return m_vmm->ReadSafe(reg,&flags);
}

//------------------------------------------------------------------------------
int RODBusy::getFifoFullFlags(std::vector<bool>& full, std::vector<bool>& empty) {
  int ret(0);
  full.clear();
  empty.clear();
  u_short reg = REG_FIFFF;
  u_short ffflags(0);
  ret = m_vmm->ReadSafe(reg,&ffflags);
  u_short feflags(0);
  reg = REG_FIFEF;
  ret |= m_vmm->ReadSafe(reg,&feflags);
  
  u_short mask = 0x1;
  for (int i = 0; i<16; ++i) {
    bool ffflag = ( (mask & ffflags)==mask ? true : false );
    bool feflag = ( (mask & feflags)==mask ? true : false );
    full.push_back(ffflag);
    empty.push_back(feflag);
    mask = mask<<1;
  }

  return ret;
}


//------------------------------------------------------------------------------
int RODBusy::readWordFromFifos(std::vector<u_short>& words) {
  int ret = 0;
  u_short reg;
  words.clear();
  
  // test that fifos are not empty
  u_short flags(0);
  ret |= getFifoEmptyFlags(flags);
  if (flags != 0x0) {
    return(ERROR_FIFOS_EMPTY);
  }
  u_short word;
  for (int i = 0; i < 16; ++i) {
    reg = REG_FIFO+i*0x2;
    ret |= m_vmm->ReadSafe(reg,&word);
    words.push_back(word);
  }
  return ret;
}

//------------------------------------------------------------------------------
int RODBusy::readWordFromFifo(u_short channel, u_short& word) {
  int ret(0);
  if (channel>16) {
    return(ERROR_CHANNEL_OUTOFRANGE);
  }
  
  // test that fifos are not empty
  u_short flags(0);
  ret |= this->getFifoEmptyFlags(flags);
  u_short mask = 0x1 << channel;
  if ((flags & mask) == mask) {
    return(ERROR_FIFOS_EMPTY);
  }

  u_short reg = REG_FIFO+channel*0x2;
  ret |= m_vmm->ReadSafe(reg, &word);
  return ret;
}

//------------------------------------------------------------------------------
int RODBusy::readFractionFromFifos(std::vector<double>& fracs) {
  int ret = 0;
  u_short reg;
  fracs.clear();

  u_short interval;
  if (ret |= this->getSequencerTransferInterval(interval) != 0) {
    return ret;
  }
  // need to increment interval by one to get the proper fraction
  ++interval;

  // test that fifos are not empty
  u_short flags(0);
  ret |= getFifoEmptyFlags(flags);
  if (flags != 0x0) {
    return(ERROR_FIFOS_EMPTY);
  }
  u_short word;
  for (int i = 0; i < 16; ++i) {
    reg = REG_FIFO+i*0x2;
    ret |= m_vmm->ReadSafe(reg,&word);
    fracs.push_back(static_cast<double>(word)/static_cast<double>(interval));
  }
  return ret;
}

//------------------------------------------------------------------------------
int RODBusy::readFractionFromFifo(u_short channel, double & frac) {
  int ret(0);
  if (channel>16) {
    return(ERROR_CHANNEL_OUTOFRANGE);
  }
  
  u_short interval;
  if (ret |= this->getSequencerTransferInterval(interval) != 0) {
    return ret;
  }
  // need to increment interval by one to get the proper fraction
  ++interval;
  
  // test that fifos are not empty
  u_short flags(0);
  ret |= getFifoEmptyFlags(flags);
  u_short mask = 0x1 << channel;
  if ((flags & mask) == mask) {
    return(ERROR_FIFOS_EMPTY);
  }

  u_short reg = REG_FIFO+channel*0x2;
  u_short word(0);

  ret |= m_vmm->ReadSafe(reg, &word);

  frac = static_cast<double>(word)/static_cast<double>(interval);

  return ret;
}

//------------------------------------------------------------------------------
int RODBusy::dumpWordsFromFifos(std::vector< std::vector<u_short> >& wordsPerChannel, u_int& nwords) {
  int ret(0);
  // read from all FIFOs until they are empty or at most 512 times
  wordsPerChannel.clear();
  wordsPerChannel.resize(16);
  
  nwords = 0;
  u_short flags(0);
  bool notempty(false);

  this->getFifoEmptyFlags(flags);
  if (flags |= 0x0) {
    notempty = false;
  } else {
    notempty = true;
  }

  while ( (nwords < 512) && notempty ) {
    for (int ch = 0; ch < 16; ++ch) {
      // read from fifo
      u_short reg = REG_FIFO+ch*0x2;
      u_short word(0);
      ret |= m_vmm->ReadSafe(reg, &word);
      wordsPerChannel[ch].push_back(word);
    }
    ++nwords;

    this->getFifoEmptyFlags(flags);
    if (flags |= 0x0) {
      notempty = false;
    } else {
      notempty = true;
    }
  }
  return ret;
}

//------------------------------------------------------------------------------
int RODBusy::dumpFractions(std::vector<double>& fracs, u_int& nwords) {

  int ret = 0;
  u_short interval;

  if (ret |= this->getSequencerTransferInterval(interval) != 0) {
    return ret;
  }
  // need to increment interval by one to get the proper fraction
  ++interval;

  // read from all FIFOs until they are empty or at most 512 times
  std::vector< std::vector<u_short> > wordsPerChannel;
  ret = dumpWordsFromFifos(wordsPerChannel, nwords);

  fracs.clear();

  for (int ch=0; ch<16; ++ch) {
    double fraction = 0.0;
    for (std::vector<u_short>::const_iterator it = wordsPerChannel[ch].begin();
	 it != wordsPerChannel[ch].end();
	 ++it) {
      fraction += static_cast<double>(*it)/static_cast<double>(interval);
    }
    fraction = fraction /static_cast<double>(wordsPerChannel[ch].size());
    fracs.push_back(fraction);
  }

  return(ret);
}

//------------------------------------------------------------------------------
int RODBusy::dumpIRQ() {
  int ret(0);

  std::cout << std::endl;
  std::cout << ">>> VMEbus interrupt status <<<" << std::endl;

  // Interrupt vector number
  u_short id(0);
  ret |= this->get_statusID(id);
  printf(" Interrupt vector number (status id) = %d (0x%02x)\n", id, id);
  
  // Interrupter control register
  bool irqen = this->IRQ_is_enabled();
  bool irqpend = this->IRQ_is_pending();
  if (irqen) {
    std::cout << " VMEbus interrupt is enabled" << std::endl;
  } else {
    std::cout << " VMEbus interrupt is disabled" << std::endl;
  }
  if (irqpend) {
    std::cout << " VMEbus interrupt is pending" << std::endl;
  } else {
    std::cout << " VMEbus interrupt is not pending" << std::endl;
  }
  return(ret);
}

int RODBusy::dumpSREQ() {
  int ret(0);

  std::cout << std::endl;
  std::cout << ">>> Service request status <<<" << std::endl;

  bool sreqpend = this->SREQ_is_pending();
  if (sreqpend) {
    std::cout << " Service request is pending" << std::endl;
  } else {
    std::cout << " Service request is not pending" << std::endl;
  }

  bool sreqen = this->SREQ_is_enabled();
  bool sreqact = this->SREQ_is_active();
  if (sreqen) {
    std::cout << " Service request is enabled" << std::endl;
  } else {
    std::cout << " Service request is not enabled" << std::endl;
  }
  if (sreqact) {
    std::cout << " Service request is active" << std::endl;
  } else {
    std::cout << " Service request is not active" << std::endl;
  }

  u_short limit(0);
  u_short interval(0);
  ret |= this->getLimit(limit);
  ret |= this->getResetInterval(interval);
  printf(" Service request limit count = %d (0x%04x)\n", limit, limit);
  printf(" Service request reset interval = %d (0x%04x)\n", interval, interval);

  return(ret);
}

int RODBusy::dumpBusy() {
  int ret(0);

  std::cout << std::endl;
  std::cout << ">>> Busy status <<<" << std::endl;

  bool isbusy = this->is_busy();
  bool isswbusy = this->sw_busy_is_asserted();
  if (isbusy) {
    std::cout << " RODBusy module is busy" << std::endl;
  } else {
    std::cout << " RODBusy module is not busy" << std::endl;
  }
  if (isswbusy) {
    std::cout << " Software busy is asserted" << std::endl;
  } else {
    std::cout << " Software busy is not asserted" << std::endl;
  }

  u_short busystate(0);
  ret |= this->getBusyState(busystate);
  u_short busymask(0);
  ret |= this->getBusyMask(busymask);

  printf(" Channel         111111          \n");
  printf("                 5432109876543210\n");
  printf(" Mask    0x%04x  ", busymask);
  bool bit(false);
  for (u_short ch = 16; ch !=0; --ch) {
    bit = channelIsEnabled(ch-1);
    std::cout << bit;
  }
  std::cout << std::endl;
  printf(" State   0x%04x  ", busystate);
  bit = false;
  for (u_short ch = 16; ch !=0; --ch) {
    ret |= this->getBusyState(bit,ch-1);
    std::cout << bit;
  }
  std::cout << std::endl;
  return(ret);
}

int RODBusy::dumpFIFOStatus() {
  int ret(0);

  std::cout << std::endl;
  std::cout << ">>> FIFO + sequencer status <<<" << std::endl;

  enum fifomode mode;
  ret = this->getMode(mode);
  switch(mode) {
  case SEQ_COUNT_DISABLED:
    std::cout << " Sequencer + Counters disabled, CNTRST + FIFRST + FIFWEN enabled" << std::endl;
    break;
  case SEQ_DISABLED_COUNT_ENABLED:
    std::cout << " Sequencer disabled, Counters + CNTRST + FIFRST + FIFWEN enabled" << std::endl;
    break;
  case SEQ_IDLE_COUNT_DISABLED:
    std::cout << " Sequencer idle, Counters + CNTRST + FIFWEN disabled, FIFRST enabled" << std::endl;
    break;
  case SEQ_COUNT_ENABLED:
    std::cout << " Sequencer + Counter + FIFRST enabled, CNTRST + FIFWEN diabled" << std::endl;
    break;
  default:
    std::cout << " Unknown mode " << mode << std::endl;
    break;
    
  }  

  u_short seqinterval(0);
  ret |= this->getSequencerTransferInterval(seqinterval);
  printf(" Sequencer transfer interval = %d (0x%04x)\n", seqinterval, seqinterval);

  printf(" Channel         111111          \n");
  printf("                 5432109876543210\n");

  std::vector<bool> full;
  std::vector<bool> empty;
  ret |= this->getFifoFullFlags(full, empty);

  u_short fullflags(0);
  u_short emptyflags(0);
  ret |= this->getFifoFullFlags(fullflags);
  ret |= this->getFifoEmptyFlags(emptyflags);
  printf(" Empty   0x%04x  ", emptyflags);
  for (u_short ch = 16; ch !=0; --ch) {
    std::cout << empty[ch-1];
  }
  std::cout << std::endl;
  printf(" Full    0x%04x  ", fullflags);
  for (u_short ch = 16; ch !=0; --ch) {
    std::cout << full[ch-1];
  }
  std::cout << std::endl;

  return(ret);
}

int RODBusy::dump() {
  int ret(0);
  ret |= dumpIRQ();
  ret |= dumpSREQ();
  ret |= dumpBusy();
  ret |= dumpFIFOStatus();
  return(ret);
}
