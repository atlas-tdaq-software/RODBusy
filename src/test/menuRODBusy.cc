//******************************************************************************
  // file: menuRODBusy.cc
  // desc: menu for test of library for RODBusy
  //******************************************************************************

#include <fstream>
#include <cstdio>
#include <cerrno>
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <list>

#include "RCDMenu/RCDMenu.h"
#include "RODBusy/RODBusy.h"
#include "DFDebug/DFDebug.h"
#include "cmdl/cmdargs.h"

  using namespace RCD;

// global variable
RODBusy* rodbusy;
u_int base = 0;

class RODBusyOpen : public MenuItem {
public:
  RODBusyOpen() { setName("Open RODBusy"); }
  int action() {
    int status = 0;

    rodbusy = new RODBusy();

    status = rodbusy->Open(base);

    u_int board_man = 0;
    u_int board_revision = 0;
    u_int board_id = 0;
    status |= rodbusy->ReadConfigurationEEPROM(board_man, board_revision, board_id);

    printf("\n");
    printf(" Manufacturer: 0x%08x (CERN = 0x00080030)\n", board_man);
    printf(" Revision    : 0x%08x \n", board_revision);
    printf(" Id          : 0x%08x \n", board_id);
    printf("\n");

    bool isrodbusy = rodbusy->CheckRODBusy();
    if (isrodbusy) {
      printf(" RODBusy detected at base address 0x%06x\n", base);
    } else {
      printf(" ERROR: RODBusy check failed. Check base address 0x%06x.\n", base);
    }

    return status;
  }
};

//------------------------------------------------------------------------------
class RODBusyClose : public MenuItem {
public:
  RODBusyClose() { setName("Close RODBusy"); }
  int action() {
    int status = rodbusy->Close();
    delete rodbusy;
    return(status);
  }
};


//------------------------------------------------------------------------------
class SWReset : public MenuItem {
public:
  SWReset() { setName("Software Reset"); }
  int action() {
    return(rodbusy->Reset());
  }
};

//------------------------------------------------------------------------------
class ProvokeInterrupt : public MenuItem {
public:
  ProvokeInterrupt() { setName("Provoke interrupt"); }
  int action() {
    return(rodbusy->provokeInterrupt());
  }
};

//------------------------------------------------------------------------------
class WriteIRQVector : public MenuItem {
public:
  WriteIRQVector() { setName("Write IRQ Vector (Status ID)"); }
  int action() {
    int id = enterHex("Enter IRQ vector (status id)", 1, 0xff);
    return(rodbusy->set_statusID(id));
  }
};

//------------------------------------------------------------------------------
class EnableIRQ : public MenuItem {
public:
  EnableIRQ() { setName("Enable IRQ"); }
  int action() {
    int ret(0);
    
    int sel = enterInt("Enable IRQ (-1=quit, 0=disable, 1=enable)", -1, 1);

    if (sel == -1) {
      return(ret);
    } else if (sel == 0) {
      ret = rodbusy->disable_IRQ();
    } else if (sel == 1) {
      ret = rodbusy->enable_IRQ();
    }

    return(ret);
  }
};

//------------------------------------------------------------------------------
class DumpIRQ : public MenuItem {
public:
  DumpIRQ() { setName("Dump"); }
  int action() {
    return(rodbusy->dumpIRQ());
  }
};

//------------------------------------------------------------------------------
class SetSWBusy : public MenuItem {
public:
  SetSWBusy() { setName("Software Busy"); }
  int action() {
    int ret(0);
    int sel = enterInt("Software busy (-1=quit, 0=release, 1=set)", -1, 1);
    if (sel==-1) {
      return(ret);
    } else if (sel == 0) {
      ret |= rodbusy->release_sw_busy();
    } else if (sel == 1) {
      ret |= rodbusy->assert_sw_busy();
    }

    return(ret);
  }
};

//------------------------------------------------------------------------------
class EnableSRQ : public MenuItem {
public:
  EnableSRQ() { setName("Enable Service Requests"); }
  int action() {
    int ret(0);
    int sel = enterInt("Service Requests (-1=quit, 0=disable, 1=enable)", -1, 1);
    if (sel==-1) {
      return(ret);
    } else if (sel == 0) {
      ret |= rodbusy->disable_SREQ();
    } else if (sel == 1) {
      ret |= rodbusy->enable_SREQ();
    }
    return(ret);
  }
};

//------------------------------------------------------------------------------
class SetSREQ : public MenuItem {
public:
  SetSREQ() { setName("Set Service Request"); }
  int action() {
    return(rodbusy->assert_SREQ());
  }
};

//------------------------------------------------------------------------------
class ClearSREQ : public MenuItem {
public:
  ClearSREQ() { setName("Clear Service Request"); }
  int action() {
    return(rodbusy->clear_SREQ());
  }
};

//------------------------------------------------------------------------------
class SetLimit : public MenuItem {
public:
  SetLimit() { setName("Set Limit"); }
  int action() {
    int ret(0);
    int limit = enterInt("Enter limit (-1=quit)", -1, 0xffff);
    if (limit==-1) {
      return(ret);
    }
    return(rodbusy->setLimit(static_cast<u_short>(limit)));
  }
};

//------------------------------------------------------------------------------
class SetInterval : public MenuItem {
public:
  SetInterval() { setName("Set Interval"); }
  int action() {
    int ret(0);
    int interval = enterInt("Enter limit (-1=quit)", -1, 0xffff);
    if (interval==-1) {
      return(ret);
    }
    return(rodbusy->setResetInterval(static_cast<u_short>(interval)));
  }
};

//------------------------------------------------------------------------------
class DumpSREQ : public MenuItem {
public:
  DumpSREQ() { setName("Dump"); }
  int action() {
    return(rodbusy->dumpSREQ());
  }
};


//------------------------------------------------------------------------------
class DumpStatus : public MenuItem {
public:
  DumpStatus() { setName("Dump"); }
  int action() {
    return(rodbusy->dump());
  }
};

//------------------------------------------------------------------------------
class SetBusyStateHex : public MenuItem {
public:
  SetBusyStateHex() { setName("Set Busy State (hex)"); }
  int action() {
    int mask = enterHex("Enter busy state", 0, 0xffff);
    return(rodbusy->setBusyState(static_cast<u_short>(mask)));
  }
};


//------------------------------------------------------------------------------
class SetBusyStatePerChannel : public MenuItem {
public:
  SetBusyStatePerChannel() { setName("Set Busy State (per channel)"); }
  int action() {
    int ret(0);
    int channel = enterInt("Enter channel (-1=quit)", -1, 15);
    if (channel != -1) {
      int sel = enterInt("Enter busy (0=release, 1=set)", 0, 1);
      if (sel == 0) {
	ret |= rodbusy->setBusyState(false, static_cast<u_short>(channel));
      } else if (sel == 1) {
	ret |= rodbusy->setBusyState(true, static_cast<u_short>(channel));
      }
    }
    return(ret);
  }
};


//------------------------------------------------------------------------------
class SetBusyMaskHex : public MenuItem {
public:
  SetBusyMaskHex() { setName("Set Busy Mask (hex)"); }
  int action() {
    int mask = enterHex("Enter busy mask", 0, 0xffff);
    return(rodbusy->setBusyMask(static_cast<u_short>(mask)));
  }
};


//------------------------------------------------------------------------------
class SetBusyMaskPerChannel : public MenuItem {
public:
  SetBusyMaskPerChannel() { setName("Set Busy Mask (per channel)"); }
  int action() {
    int ret(0);
    int channel = enterInt("Enter channel (-1=quit)", -1, 15);
    if (channel != -1) {
      int sel = enterInt("Enable channel (0=disable, 1=enable)", 0, 1);
      if (sel == 0) {
	ret |= rodbusy->disableChannel(static_cast<u_short>(channel));
      } else if (sel == 1) {
	ret |= rodbusy->enableChannel(static_cast<u_short>(channel));
      }
    }
    return(ret);
  }
};

//------------------------------------------------------------------------------
class DumpBusy : public MenuItem {
public:
  DumpBusy() { setName("Dump"); }
  int action() {
    return(rodbusy->dumpBusy());
  }
};


//------------------------------------------------------------------------------
class CounterReset : public MenuItem {
public:
  CounterReset() { setName("Reset Counters"); }
  int action() {
    return(rodbusy->resetCounter());
  }
};


//------------------------------------------------------------------------------
class FifoReset : public MenuItem {
public:
  FifoReset() { setName("Reset FIFOs"); }
  int action() {
    return(rodbusy->resetFifos());
  }
};


//------------------------------------------------------------------------------
class Transfer : public MenuItem {
public:
  Transfer() { setName("Transfer Counter Data to FIFOs"); }
  int action() {
    return(rodbusy->transferCountersToFifos());
  }
};


//------------------------------------------------------------------------------
class SetMode : public MenuItem {
public:
  SetMode() { setName("Counter/FIFO Control Mode"); }
  int action() {
    std::cout << "Counter/FIFO Control Modes:" << std::endl;
    std::cout << RODBusy::SEQ_COUNT_DISABLED << ": Sequencer + Counters disabled, CNTRST + FIFRST + FIFWEN enabled" << std::endl;
    std::cout << RODBusy::SEQ_DISABLED_COUNT_ENABLED << ": Sequencer disabled, Counters + CNTRST + FIFRST + FIFWEN enabled" << std::endl;
    std::cout << RODBusy::SEQ_IDLE_COUNT_DISABLED << ": Sequencer idle, Counters + CNTRST + FIFWEN disabled, FIFRST enabled" << std::endl;
    std::cout << RODBusy::SEQ_COUNT_ENABLED << ": Sequencer + Counter + FIFRST enabled, CNTRST + FIFWEN diabled" << std::endl;
    int sel = enterInt("Enter mode number", 0, 3);
    
    return(rodbusy->setMode(static_cast<enum RODBusy::fifomode>(sel)));
  }
};


//------------------------------------------------------------------------------
class FifoReadControl : public MenuItem {
public:
  FifoReadControl() { setName("FIFO Read Control"); }
  int action() {
    int sel = enterInt("Enter (-1=quit, 0=VME, 1=Sequencer)", -1, 1);
    u_short mask(0);
    if (sel == -1) {
      return(0);
    } else if (sel == 0) {
      mask = 0x0;
    } else {
      mask = 0xffff;
    }
    return(rodbusy->setFifoReadingByVME(mask));
  }
};


//------------------------------------------------------------------------------
class TransferInterval : public MenuItem {
public:
  TransferInterval() { setName("Sequencer Transfer interval"); }
  int action() {
    int sel = enterInt("Enter transfer interval (-1=quit)", -1, 0xffff);
    if (sel == -1) {
      return(0);
    }
    return(rodbusy->setSequencerTransferInterval(static_cast<u_short>(sel)));
  }
};


//------------------------------------------------------------------------------
class ReadFromFifo : public MenuItem {
public:
  ReadFromFifo() { setName("Read words from FIFO"); }
  int action() {
    int ret(0);
    // channels
    int sel = enterInt("Enter channel", 0, 15);
    // how many words
    int nwords = enterInt("Enter number of words (-1=until empty)", -1, 512);
    std::vector<bool> full;
    std::vector<bool> empty;
    u_short word;
    u_int count = 0;
    u_short interval(0);
    ret |= rodbusy->getSequencerTransferInterval(interval);
    // need to increment by one
    ++interval;
    float frac(0);

    while ((static_cast<int>(count)<nwords) || (nwords==-1) ) {
      // if fifo not empty, read from fifo
      ret |= rodbusy->getFifoFullFlags(full, empty);
      if (!empty[sel]) {
	ret |= rodbusy->readWordFromFifo(static_cast<u_short>(sel), word);
	if (interval==0) {
	  frac=0;
	} else {
	  frac = static_cast<float>(word)/static_cast<float>(interval);
	}
	printf(" word %d: %d (0x%04x) fraction=%5.4f\n", count, word, word, frac);
	++count;
      } else {
	return(ret);
      }
    }

    return(ret);
  }
};

//------------------------------------------------------------------------------
class ReadFractions : public MenuItem {
public:
  ReadFractions() { setName("Read fractions"); }
  int action() {
    int ret(0);
    int sel = enterInt("Enter accumulation period", 1, 10000);
    u_short interval(0);
    ret |= rodbusy->getSequencerTransferInterval(interval);
    // need to increment by one
    ++interval;
    std::vector<float> frac(16);
    u_short empty(0);
    int ic(0);
    std::vector<u_short> fifowords;
    while(true) {
      // accumulate if fifo is not empty
      ret |= rodbusy->getFifoEmptyFlags(empty);
      if (empty == 0x0) {
	++ic;
	ret |= rodbusy->readWordFromFifos(fifowords);
	for (int i = 0; i<16; ++i) {
	  frac[i] += static_cast<float>(fifowords[i])/static_cast<float>(interval);
	}
	// if accumlation period is reached, make printout
	if (ic == sel) {
	  for (int i = 0; i<16; ++i) {
	    ic = 0;
	    frac[i] = frac[i]/static_cast<float>(sel);
	    printf(" Channel %2d: %7.5f\n", i, frac[i]);
	    frac[i] = 0;
	  }
	  printf("\n");
	}
      
      }
    }
    
    return(ret);
  }
};


//------------------------------------------------------------------------------
class DumpFifoStatus : public MenuItem {
public:
  DumpFifoStatus() { setName("Dump"); }
  int action() {
    return(rodbusy->dumpFIFOStatus());
  }
};




//------------------------------------------------------------------------------

int main(int argc, char** argv) {

  CmdArgStr  base_cmd('a', "address", "vme-base-address",  "VME base address in hex", CmdArg::isOPT);
  base_cmd = "";

  CmdLine  cmd(*argv, &base_cmd, nullptr);
  CmdArgvIter  arg_iter(--argc, ++argv);
  cmd.description("Command-line driven program to communicate with a RODBusy board");
  cmd.parse(arg_iter);

  if (base_cmd=="") {
    // no argument given, ask for base address
    std::string in;
    std::cout << "<base>?" << std::endl;
    getline(cin,in);
    if(sscanf(in.c_str(),"%x",&base) == EOF) exit(EINVAL);
    if(base < 0x100) base = base << 16;
  } else {
    // get first argument, the base address
    std::string in(base_cmd);
    sscanf(argv[1], "%x", &base);
    if(base < 0x100) base = base << 16;
  }

  printf(" VME base adresss = 0x%06x\n", base);

  Menu	menu("RODBusy Main Menu");

  // generate menu
  menu.init(new RODBusyOpen);

  menu.add(new DumpStatus);

  Menu menuIRQ("VME Interrupts");
  menuIRQ.add(new DumpIRQ);
  menuIRQ.add(new SWReset);
  menuIRQ.add(new ProvokeInterrupt);
  menuIRQ.add(new WriteIRQVector);
  menuIRQ.add(new EnableIRQ);
  menu.add(&menuIRQ);

  Menu menuSREQ("Service Requester");
  menuSREQ.add(new DumpSREQ);
  menuSREQ.add(new EnableSRQ);
  menuSREQ.add(new SetSREQ);
  menuSREQ.add(new ClearSREQ);
  menuSREQ.add(new SetLimit);
  menuSREQ.add(new SetInterval);
  menu.add(&menuSREQ);

  Menu menuBusy("Busy");
  menuBusy.add(new DumpBusy);
  menuBusy.add(new SetSWBusy);
  menuBusy.add(new SetBusyStateHex);
  menuBusy.add(new SetBusyStatePerChannel);
  menuBusy.add(new SetBusyMaskHex);
  menuBusy.add(new SetBusyMaskPerChannel);
  menu.add(&menuBusy);

  Menu menuFIFO("Counter and FIFO Sequencer");
  menuFIFO.add(new DumpFifoStatus);
  menuFIFO.add(new CounterReset);
  menuFIFO.add(new FifoReset);
  menuFIFO.add(new Transfer);
  menuFIFO.add(new SetMode);
  menuFIFO.add(new FifoReadControl);
  menuFIFO.add(new TransferInterval);
  menuFIFO.add(new ReadFromFifo);
  menuFIFO.add(new ReadFractions);
  menu.add(&menuFIFO);

  menu.exit(new RODBusyClose);

  // start menu
  menu.execute();

  return 0;
}


