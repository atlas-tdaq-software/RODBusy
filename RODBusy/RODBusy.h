#ifndef RODBusy_H
#define RODBusy_H

#include <string>
#include <vector>
#include "RCDVme/RCDVme.h"

using namespace std;

namespace RCD {

  class RODBusy {

  public:
    RODBusy() {
    }
    ~RODBusy() {}

    // low-level interface:
    std::string GetError(int errcode);
    int Reset();
    int Open(u_int base);
    int Close();
    int ReadConfigurationEEPROM(u_int& board_man, u_int& board_revision, u_int& board_id);
    bool CheckCERNID();

    // performs basic test to increase the likeliness that the vme
    // module is actually an RODBusy module
    bool CheckRODBusy();

    enum error_codes {
      ERROR_IRQ_LEVEL_OUTOFRANGE = 0x8010,
      ERROR_FIFOS_EMPTY          = 0x8020,
      ERROR_CHANNEL_OUTOFRANGE   = 0x8040
    };

    enum fifomode { SEQ_COUNT_DISABLED, 
		    SEQ_DISABLED_COUNT_ENABLED, 
		    SEQ_IDLE_COUNT_DISABLED, 
		    SEQ_COUNT_ENABLED };

    // ----------------
    // VMEbus Interrupt
    // ----------------
    int provokeInterrupt();
    int set_statusID(u_short id);
    int get_statusID(u_short& id);
    
    int set_IRQ_level(u_short l);
    int get_IRQ_level(u_short& l);
    
    int enable_IRQ();
    int disable_IRQ();

    bool IRQ_is_enabled();
    bool IRQ_is_disabled();
    bool IRQ_is_pending();
    bool SREQ_is_pending();

    bool is_busy();
    bool is_not_busy();
    int assert_sw_busy();
    int release_sw_busy();
    bool sw_busy_is_asserted();
    int enable_SREQ();
    int disable_SREQ();
    bool SREQ_is_enabled();
    bool SREQ_is_active();

    int assert_SREQ();
    int clear_SREQ();
    
    int setLimit(u_short l);
    int getLimit(u_short& l);

    int setResetInterval(u_short l);
    int getResetInterval(u_short& l);

    int getBusyState(u_short& l);
    int setBusyState(u_short l);
    int getBusyState(bool& b, u_short channel);
    int setBusyState(bool b, u_short channel);

    int setBusyMask(u_short l);
    int getBusyMask(u_short& l);
    int enableChannel(u_short channel);
    int disableChannel(u_short channel);
    bool channelIsEnabled(u_short channel);

    int resetCounter();
    int resetFifos();
    int transferCountersToFifos();
    int setMode(enum fifomode m);
    int getMode(enum fifomode& m);

    int setFifoReadingByVME(u_short hex);
    int getFifoReadingByVME(u_short& hex);
    int getSequencerTransferInterval(u_short& interval);
    int setSequencerTransferInterval(u_short interval);
    int getFifoFullFlags(u_short& flags);
    int getFifoEmptyFlags(u_short& flags);
    int getFifoFullFlags(std::vector<bool>& full, std::vector<bool>& empty);

    int readWordFromFifos(std::vector<u_short>& words);
    int readWordFromFifo(u_short channel, u_short& word);
    int readFractionFromFifos(std::vector<double>& fracs);
    int readFractionFromFifo(u_short channel, double& fracs);
    int dumpWordsFromFifos(std::vector< std::vector<u_short> >& wordsPerChannel, u_int& nwords);
    int dumpFractions(std::vector<double>& fracs, u_int& nwords);

    int dump();
    int dumpIRQ();
    int dumpSREQ();
    int dumpBusy();
    int dumpFIFOStatus();

  private:

    // helper methods
    int  setBits(u_int reg, u_short mask, u_short bits);
    int  getBits(u_int reg, u_short mask, u_short& bits);
    int  testBits(u_int reg, u_short mask, bool& result);

    // register addresses
    static const u_short REG_FIFEF          = 0xee;
    static const u_short REG_FIFFF          = 0xec;
    static const u_short REG_SEQREG         = 0xea;
    static const u_short REG_FIFRCR         = 0xe8;
    static const u_short REG_FIFWCR         = 0xe6;
    static const u_short REG_FIFWEN         = 0xe4;
    static const u_short REG_FIFRST         = 0xe2;

    static const u_short REG_CNTRST         = 0xe0;
    static const u_short REG_FIFO           = 0xc0;// 0xc0-0xde
    static const u_short REG_BUSYMASK       = 0x9a;
    static const u_short REG_BUSYSTATE      = 0x98;
    static const u_short REG_IVALREG        = 0x96;
    static const u_short REG_LIMREG         = 0x94;
    static const u_short REG_SREQSETCLR     = 0x92;
    static const u_short REG_SREQCSR        = 0x90;
    
    static const u_short REG_INTID          = 0x86;// RW W 16 VME Intrp Status/ID register VME IF
    static const u_short REG_INTCSR         = 0x84;// RW W 16 VME Interrupter CSR
    static const u_short REG_SWIRQ          = 0x82;//  W W - VME Interrupt by soft function
    static const u_short REG_SWRST          = 0x80;//  W W - Reset module / data_less function
    static const u_short REG_Cfg_EEPROM     = 0x00;// R(W) W 16 LSBytes in every Long Word. See specs.


    // mask & data: SWRST
    static const u_short MSK_SWRST              = 0xffff;
    static const u_short SWRST                  = 0xffff;

    // mask & data: SWRST
    static const u_short MSK_SWIRQ              = 0xffff;
    static const u_short SWIRQ                  = 0xffff;

    // mask & data: INTID
    static const u_short MSK_INTID_VECTOR       = 0x00ff;


    // mask & data: INTCSR
    static const u_short MSK_INTCSR_IRQ_R       = 0x0007;
    static const u_short MSK_INTCSR_IRQ_W       = 0x0006;
    
    static const u_short MSK_INTCSR_IRQ_EN      = 0x0008;
    static const u_short INTCSR_IRQ_EN          = 0x0008;
    static const u_short INTCSR_IRQ_DIS         = 0x0000;

    static const u_short MSK_INTCSR_IRQ_PEND    = 0x0010;
    static const u_short INTCSR_IRQ_PEND        = 0x0010;

    static const u_short MSK_INTCSR_SREQ_PEND   = 0x0020;
    static const u_short INTCSR_SREQ_PEND       = 0x0020;

    // mask & data: SREQCSR
    static const u_short MSK_SREQCSR_BSY_STATUS = 0x0001;
    static const u_short SREQCSR_BSY_STATUS     = 0x0001;
    static const u_short MSK_SREQCSR_SWBUSY     = 0x0002;
    static const u_short SREQCSR_SWBUSY_EN      = 0x0002;
    static const u_short SREQCSR_SWBUSY_DIS     = 0x0000;
    static const u_short MSK_SREQCSR_SREQ_EN    = 0x0004;
    static const u_short SREQCSR_SREQ_EN        = 0x0004;
    static const u_short SREQCSR_SREQ_DIS       = 0x0000;
    static const u_short MSK_SREQCSR_SREQ_ACT   = 0x0008;
    static const u_short SREQCSR_SREQ_ACT       = 0x0008;
    static const u_short MSK_SREQSETCLR         = 0x0003;
    static const u_short SREQSETCLR_SET         = 0x0002;
    static const u_short SREQSETCLR_CLR         = 0x0001;
    static const u_short SREQSETCLR_NOP         = 0x0003;

    // masks
    static const u_short MSK_LIMREG             = 0xffff;
    static const u_short MSK_IVALREG            = 0xffff;
    static const u_short MSK_BUSYSTATE          = 0xffff;
    static const u_short MSK_BUSYMASK           = 0xffff;
    static const u_short MSK_CNTRST             = 0xffff;
    static const u_short MSK_FIFRST             = 0xffff;
    static const u_short MSK_FIFWEN             = 0xffff;

    // masks + data
    static const u_short MSK_FIFWCR             = 0x000f;
    static const u_short FIFWCR_SDIS_CDIS       = 0x0000;
    static const u_short FIFWCR_SDIS_CEN        = 0x0001;
    static const u_short FIFWCR_SID_CDIS        = 0x0002;
    static const u_short FIFWCR_SEN_CEN         = 0x0003;

    // masks
    static const u_short MSK_FIFRCR             = 0xffff;
    static const u_short MSK_SEQREG             = 0xffff;
    static const u_short MSK_FIFFF              = 0xffff;
    static const u_short MSK_FIFEF              = 0xffff;

    // -- VMEbus driver/library and master mapping
    VME*			m_vme;
    VMEMasterMap*		m_vmm;
    static const u_int		VME_SIZE	= 0x0100;

  };

}	// namespace RCD

#endif // RODBusy_H
